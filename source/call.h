#include <nds.h>
#include <stdio.h>
#include "cui.h"
#define MAX_CUIS 5
#define MAX_CUI_X 1024
#define MAX_CUI_RES 512
#define MAX_CURVES 5
#define PITCH_CURVE 0
#define VOL_CURVE 1
#define PAN_CURVE 2
#define LFO_CURVE 3
#define LFOF_CURVE 4
#define LFOS_CURVE 4


// multiple cui tored there
  //TODO : this into a u16 later !! it has no need to be this fine
typedef struct CallS {
  Cui * cuis[MAX_CUIS];
  s16 base_volume;
  s16 base_freq;
  s16 base_lfo_freq;
  bool lfo_enabled;

  // UI
  Cui * selectedCui;
  u8  curve;
  u16 scroll_x;
  u16 scroll_y;

  //
  s16 tri_strength;
  s16 squ_strength;
  s16 sin_strength;

} Call;


Call * makeCall(){
    Call * call = malloc(sizeof(Call));
    call-> base_freq = 1000;
    call-> base_lfo_freq =  26;
    call-> base_volume  = inttof32(1);
    call-> sin_strength = floattof32(-1.);
    call-> tri_strength = floattof32(-1.);
    call-> squ_strength = floattof32(1.);
    call-> lfo_enabled=true;
    call-> selectedCui = NULL;
    call-> curve = 0;
    

    for (int i = 0; i < MAX_CUIS; i++) {
        call->cuis[i] = malloc(sizeof(Cui));
        Cui * prev = NULL;
        if (i > 0) {
        prev = call->cuis[i-1];
        }
        makeCui(call->cuis[i],i,i*20+10,50,prev);
        bakeCurves(prev);
    }
    return call;
}
void selectFirst(Call * call){
  call->selectedCui = call->cuis[0];
  call->cuis[0]->selected=true;
}


char * getCurveName(u8 curve_num) {
  switch (curve_num) {
    case 0: return "pitch";
    case 1: return "volume";
    case 2: return "pan";
    case 3: return "lfo_strength";
    case 4: return "lfo_freq";
    case 5: return "c1";
    case 6: return "c2";
    case 7: return "c3";
    case 8: return "c4";
    case 9: return "c5";
  }
  return "outflow";
}