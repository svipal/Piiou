#include <nds.h>
// #include <video.h>
#include <nds/ndstypes.h>
#include <stdio.h>
#include <maxmod9.h>
#include <gl2d.h>
#include <stdlib.h>
#include <time.h>
#include "extra_math.h"
#include "call.h"
#include "draw.h"
#define sample_rate  48000
#define HALF_WIDTH (SCREEN_WIDTH/2)
#define HALF_HEIGHT (SCREEN_HEIGHT/2)
#define BRAD_PI (1 << 14)
#define TIMER 6
#define DRAG_TIMER 6

#define MAGIC_NUM  6069
#define MAX_CURVES 5
//struct globals
Call * call;

// drawing globals
u16 cuiColor = ARGB16(1, 31, 0, 0);
u16 selectColor = ARGB16(1, 31,31, 31);
u16 playColor = ARGB16(1, 10, 31, 10);
u16 bgColor = ARGB16(1, 10, 1, 10);
u16 lineColor = ARGB16(1, 0,0,31);
int draw_x;
int draw_y;
int draw_offset;

bool lfo_enabled = false;

int cx;
int cy;
// ui globals
int scroll_x;
int scroll_y;

// audio globals (need to be global for audio stream)
s16 alpha;
s16 lfo;
s16 freq;
int lfo_freq;
int lfo_shift = 2;
u8 lfo_strength;
int volume;

int sin_volume;
int tri_volume;
int squ_volume;

int pan = floattof32(0.5);
/***********************************************************************************
 * on_stream_request
 *
 * Audio stream data request handler.
 ***********************************************************************************/
mm_word on_stream_request( mm_word length, mm_addr dest, mm_stream_formats format ) {
  //----------------------------------------------------------------------------------
	s16 *target = dest;
	//------------------------------------------------------------
	// synthesize a sine wave
	// the stereo data is interleaved
	//------------------------------------------------------------
	int len = length;
  // if (volume ==  0) {


  // }
	for( ;len; len-- )
    {
      s16 sin_sample = sinLerp(alpha);
      s16 tri_sample = triFunc(alpha);
      s16 squ_sample = squFunc(alpha, 15);
      
      s16 sample = 0;

      if (volume > 0) {
        if (sin_volume > 0){
          sample +=  mulf32(sin_sample,sin_volume);
        }
        if (tri_volume > 0) {
          // printsf  ("value : %04f",f32tofloat(tri_sample));
          sample += mulf32(tri_sample,tri_volume);
        }
        if (squ_volume > 0) {
          sample += mulf32(squ_sample,squ_volume);
        }
        sample = mulf32(sample,volume);
      }
      // output sample for left

      *target++ = mulf32(sample,one-pan);
      // output inverted sample for right
      *target++ = mulf32(sample,pan);
      s16 strength = divf32(lfo_strength,SCREEN_WIDTH-1);
      if (lfo_enabled) {
        alpha +=  freq + mulf32(strength,(sinLerp(lfo) >> lfo_shift));
      } else {
        alpha += freq;
      }
      lfo = (lfo + lfo_freq);
    }
	return length;
}

// MARGES  ""
int margin_x = 20;
int margin_y = 20;
void drawCui(u16* memory, Cui * cui) {
  s16 cui_y = getY(cui,call->curve);
  // we don't draw out of bounds
  // drawBox(videoMemoryMain,true,25,25,110,110,ARGB16(1, 0, 0, 30));
  cx=cui->x+scroll_x+margin_x;
  cy=cui_y+scroll_y-margin_y;
  if (cx > SCREEN_WIDTH-1 || cx < 0) return;
  if (cy > SCREEN_HEIGHT-1 || cy < 0) return;
  // size of the box
  int sz = 3;
  glBox(cx-sz,cy-sz,cx+sz,cy+sz,cuiColor);
  if (cui->selected) {
    sz = 2;
    glBoxFilled(cx-sz,cy-sz,cx+sz,cy+sz,selectColor);
  }
  if (cui->progress>0) {
    iprintf(">");
    draw_x = 0;
    draw_y = 0;
    draw_offset = 0;
    getPlayerPosition(cui,&draw_x,&draw_y,&draw_offset,call->curve);
    draw_x += scroll_x;
    draw_x += margin_x;
    draw_y += scroll_y;
    draw_y -= draw_offset;
    draw_y -= margin_y;
    glTriangleFilled( draw_x-3,
                draw_y-3,
                draw_x-3,
                draw_y+3,
                draw_x+3,
                draw_y,playColor);
    // drawBox(    memory,
    //             true,
    //             draw_x-1,
    //             draw_y-1,
    //             draw_x+1,
    //             draw_y+1,
    //             playColor);
  }
  // iprintf("x:%03i y: %03i\n",cx,cy);
}

void drawLine(u16 * memory, Cui * cui) {
  for (int i = 0; i < cui->curve_res;i++) {
    //using a box in case we need to zoom later
    draw_x      =  cui->curves[call->curve].values[0][i];
    draw_y      =  cui->curves[call->curve].values[1][i];
    draw_offset =  cui->curves[call->curve].offset[i];
    // no drawing out of bounds 
    if (draw_x+scroll_x < 0 || draw_x +scroll_x > SCREEN_WIDTH-1 || draw_y-draw_offset+scroll_y < 0 || draw_y-draw_offset+scroll_y > SCREEN_HEIGHT-1) continue;
    glBoxFilled(
            draw_x+scroll_x + margin_x,draw_y-draw_offset+scroll_y-margin_y,
            draw_x+scroll_x + margin_x,draw_y-draw_offset+scroll_y-margin_y,
            lineColor);
  
  }
}
int bgMain;
int bgSub;
u16 * videoMemoryMain;
u16 * videoMemorySub;

int main(void)
{
  // MAXMOD INITIALIZATION WITHOUT BANK (AUDIO STREAMING SYSTEM)
  mm_ds_system sys;
	sys.mod_count 			= 0;
	sys.samp_count			= 0;
	sys.mem_bank			  = 0;
	sys.fifo_channel		= FIFO_MAXMOD;
	mmInit( &sys );

	mm_stream mystream;
	mystream.sampling_rate	= sample_rate;
	mystream.buffer_length	= 1200;						// buffer length = 1200 samples
	mystream.callback		= on_stream_request;		// set callback function
	mystream.format			= MM_STREAM_16BIT_STEREO;	// format = stereo 16-bit
	mystream.timer			= MM_TIMER0;				// use hardware timer 0
	mystream.manual			= false;						// use manual filling
	mmStreamOpen( &mystream );

	int keys, hkeys; //touched keys, held keys

  // screen
	touchPosition touch;
	touchPosition lastTouch = {.px =0, .py =0};

  //video
  videoSetMode(MODE_0_3D);
  videoSetModeSub(MODE_5_3D);

  vramSetBankA(VRAM_A_MAIN_BG);
  vramSetBankC(VRAM_C_SUB_BG);

  bgMain = bgInit(3, BgType_Bmp16, BgSize_B16_256x256, 0,0);
  bgSub = bgInitSub(3, BgType_Bmp16, BgSize_B16_256x256, 0,0);
  videoMemoryMain = bgGetGfxPtr(bgMain);
  videoMemorySub = bgGetGfxPtr(bgSub);
	consoleDemoInit();

  lcdMainOnBottom();
  glScreen2D();
  // i have no idea what this means
	SetYtrigger( 0 );
  //we need this interrupt for smooth streaming
	irqEnable( IRQ_VCOUNT );
	irqEnable( IRQ_TIMER0 );
  call = makeCall();
  selectFirst(call);
  // 5 frames to touch without selecting
  u8 touch_timer = TIMER;
  // 20 frames to change curve
  u8 drag_timer = 0;

  int start_sw = 0;
  // selectFirst(call);
  int frame_count =0;

  int movement = DELICATE;

  while(1)
	{
    frame_count = (frame_count + 1) % 2;
    ////////////////////
    //DEBUG EXT AREA //
    // printf("%04f",f32tofloat(degreesToAngle(290)));
    // END DEBUG EXT //
    ////////////////////

    
    // movement mode
    if (frame_count == 1) {
      // // clear console
      consoleClear();
      if (movement == DELICATE) {
        iprintf("movement : DELICATE");
      } 
      else if (movement == LOCKED) {
        iprintf("movement : LOCKED");
      } 
      else if (movement == STABLE) {
        iprintf("movement : STABLE");
      }
      else if (movement == STRONG) {
        iprintf("movement : STRONG");
      }
      
      iprintf("\ncurrent curve: %s\n", getCurveName(call->curve));
      iprintf("pointer: %03i, %03i\n",touch.px,touch.py);
      iprintf("freq: : %04li\n",f32toint(mulf32(MAGIC_NUM,(inttof32(freq)))));
    }
      



    lfo_enabled = call->lfo_enabled;
    sin_volume = call->sin_strength;
    tri_volume = call->tri_strength;
    squ_volume = call->squ_strength;
    // long int w = 0;
    // while (w < 500000000) {
    //   w++;
    //   w*=1.5;
    // }
    // we first wait for the interrupt
		// swiIntrWait( 0, IRQ_VCOUNT);
    //input
		swiWaitForVBlank();
		touchRead(&touch);
		scanKeys();
		keys = keysDown();
    hkeys = keysHeld();
     // // Stream update
    // mmStreamUpdate();

    ///////////////
    //  DRAWING  //
    ///////////////

    glBegin2D();
    
    //OPTIONS
    s16 right_max = scroll_x-margin_x-30; 
    s16 left_max =  scroll_x-(SCREEN_WIDTH-30);
    //lfo strength indicator //
    // glBox(left_max,20,right_max,40,2345);
    // glBoxFilled(left_max,20,
    //             left_max+
    //             f32toint(
    //               mulf32(f32toint(right_max-left_max)
    //                     ,lfo_strength)),40,5069);
    
    //enable lfo//
    u8 pix = 20;
    glBox(scroll_x-margin_x-28, pix,scroll_x-margin_x-8, pix+20, 9000);
    if (call->lfo_enabled) {
      glBoxFilled(scroll_x-margin_x-26, pix+2,scroll_x-margin_x-10,pix+18,5069);
    }
    
    //enable sin//
    pix = 50;
    glBox(scroll_x-margin_x-28, pix,scroll_x-margin_x-8, pix+20, 9000);
    if (call->sin_strength > 0) {
      glBoxFilled(scroll_x-margin_x-26, pix+2,scroll_x-margin_x-10,pix+18,5069);
    }

    //enable tri//
    pix = 80;
    glBox(scroll_x-margin_x-28, pix,scroll_x-margin_x-8, pix+20, 9000);
    if (call->tri_strength > 0) {
      glBoxFilled(scroll_x-margin_x-26, pix+2,scroll_x-margin_x-10,pix+18,5069);
    }

    //enable squ//
    pix = 110;
    glBox(scroll_x-margin_x-28, pix,scroll_x-margin_x-8, pix+20, 9000);
    if (call->squ_strength > 0) {
      glBoxFilled(scroll_x-margin_x-26, pix+2,scroll_x-margin_x-10,pix+18,5069);
    }

    // //enable tan//
    // pix = 140;
    // glBox(scroll_x-margin_x-28, pix,scroll_x-margin_x-8, pix+20, 9000);
    // if (call->tan_strength > 0) {
    //   glBoxFilled(scroll_x-margin_x-26, pix+2,scroll_x-margin_x-10,pix+18,5069);
    // }
    
    // we draw the 0 lines
    // bottom
    glBox(scroll_x+1,scroll_y+SCREEN_HEIGHT-margin_y,SCREEN_WIDTH,scroll_y+SCREEN_HEIGHT,playColor-2321);
    
    // left margin
    glBox(scroll_x+1,0,scroll_x+margin_x,scroll_y+SCREEN_HEIGHT,playColor+2222);
    
    // drawBox(videoMemoryMain,true,0,0,SCREEN_WIDTH-1,SCREEN_HEIGHT-1,bgColor);
    for (int i = 0; i < MAX_CUIS;i++) {
      Cui * cui = call->cuis[i];
      if (cui != NULL) {
        // char *message = malloc(10* sizeof( char));
        // sprintf(message,"drawing %01i",i);
        // nocashMessage(message);
        if (cui->next) {
          drawLine(videoMemoryMain,cui);
        }
        drawCui(videoMemoryMain,cui);
      }
    }
    glEnd2D();
    if(keys & KEY_START && call->selectedCui)
		{
      call->selectedCui->playing=!call->selectedCui->playing;  
    }
    bool playing = false;
    for (int i = 0; i < MAX_CUIS; i++) {
      // we adjust the playback to the current cui's parameters
      if (call->cuis[i]->playing) {
        playing=true;
        // 1 PITCH //
        int prog_y = 0;
        int prog_x = 0;
        int offset = 0;
        getPlayerPosition(call->cuis[i],&prog_x,&prog_y,&offset,PITCH_CURVE);
        prog_y -= offset;
        // so we can test end nodes freq
        if (prog_x == 0) {
          prog_y = getY(call->cuis[i],call->curve)-offset;
        }
        freq = mulf32(call->base_freq,divf32(max(0,SCREEN_HEIGHT-prog_y),96));
        
        // 2 VOL //
        getPlayerPosition(call->cuis[i],&prog_x,&prog_y,&offset,VOL_CURVE);
        prog_y -= offset;
        volume = mulf32(call->base_volume,divf32(max(0,SCREEN_HEIGHT-1-prog_y),96));

        // 3 PAN //
        getPlayerPosition(call->cuis[i],&prog_x,&prog_y,&offset,PAN_CURVE);
        prog_y -= offset;
        pan = mulf32(floattof32(0.5),divf32(max(0,SCREEN_HEIGHT-1-prog_y),96));

        // 4  LFO STRENGTH//
        getPlayerPosition(call->cuis[i],&prog_x,&prog_y,&offset,LFO_CURVE);
        prog_y -= offset;
        lfo_strength = mulf32(call->base_volume /2 ,divf32(max(0,SCREEN_HEIGHT-1-prog_y),96));
        
        // 5  LFO FREQ//
        getPlayerPosition(call->cuis[i],&prog_x,&prog_y,&offset,LFOF_CURVE);
        prog_y -= offset;
        lfo_freq = mulf32(call->base_lfo_freq,divf32(max(0,SCREEN_HEIGHT-1-prog_y),96));

        //LFO shift moved to bird options ?
        
        advance(call->cuis[i]);
      }
    }
    if (!playing) {
      alpha = 0;
      lfo = 0;
      volume = 0;
    }
    ///////////
    // INPUT //
    ///////////
    if (keys & KEY_TOUCH) {
      // a timer for how many frames we have to touch a node before being allowed to move it
      touch_timer = TIMER;
      // if and only if touch was relaxed we set a new lastTouch
      if (lastTouch.px == 0 && lastTouch.py == 0) {
        lastTouch = touch;
      }

      // we don' t reset the currently selected node on a new touch if we are either
      // drawing on the  (current) curve, moving the map around, or editing the curve
      if (!(hkeys & KEY_A || hkeys & KEY_X || hkeys & KEY_B)) {
        call->selectedCui=NULL;
      
        //We try to select Cuis
        for (int i = 0; i < MAX_CUIS ;i++) {
          //if we already selected a Node we clear all other nodes;
          if (call->selectedCui  ) break;
          if (call->cuis[i]->selected) {
            call->selectedCui = call->cuis[i];
          }
          //we see if we selected a cui
          if (selectCui(call->cuis[i],  touch.px-scroll_x-margin_x,touch.py-scroll_y+margin_y, call->curve)) {
            // printf("hello");
            call->selectedCui = call->cuis[i];
          }
        }

      }
      //we try to select options

      //lfo enabled
      u8 pix = 20;
      if (touch.px >= scroll_x-margin_x-28 && touch.px <= scroll_x-margin_x-8 &&
          touch.py >=pix && touch.py <= pix+20) {
            call->lfo_enabled = !call->lfo_enabled;
          }
        
      //sine strength
      pix=50;
      if (touch.px >= scroll_x-margin_x-28 && touch.px <= scroll_x-margin_x-8 &&
          touch.py >=pix && touch.py <= pix+20) {
            call->sin_strength = -call->sin_strength;
          }

      //tri strength
      pix=80;
      if (touch.px >= scroll_x-margin_x-28 && touch.px <= scroll_x-margin_x-8 &&
          touch.py >=pix && touch.py <= pix+20) {
            call->tri_strength = -call->tri_strength;
          }

      //square strength
      pix=110;
      if (touch.px >= scroll_x-margin_x-28 && touch.px <= scroll_x-margin_x-8 &&
          touch.py >=pix && touch.py <= pix+20) {
            call->squ_strength = -call->squ_strength;
          }
    }

    if (hkeys & KEY_TOUCH) {
      int dx = touch.px-lastTouch.px;
      int dy = touch.py-lastTouch.py;


      touch_timer = max(0,touch_timer-1);

      if (hkeys & KEY_X) {
        scroll_x = clamp(-1024,scroll_x+dx,SCREEN_WIDTH-margin_x); 
        scroll_y = clamp(-5,scroll_y+dy,192); 
      } if (hkeys & KEY_X) {
        
      } else if (hkeys & KEY_A) {
        if (hkeys & KEY_L) {
          // mute(call->selectedCui, touch.px - scroll_x, touch.py);
        } else {
          // it's too fine with just one pixel being adjusted
          if (lastTouch.px - touch.px > 1) {
            for (int x = lastTouch.px;x != touch.px; x ++)
              paintCurve(call->selectedCui, x   - scroll_x - margin_x, touch.py - scroll_y + margin_y, call->curve);          
          } else if (lastTouch.px - touch.px < 1) {
            for (int x = lastTouch.px;x != touch.px; x++)
              paintCurve(call->selectedCui, x   - scroll_x - margin_x, touch.py - scroll_y + margin_y, call->curve);          
          }

          paintCurve(call->selectedCui, touch.px   - scroll_x - margin_x, touch.py - scroll_y + margin_y, call->curve);
          // paintCurve(call->selectedCui, touch.px+1 - scroll_x - margin_x, touch.py - scroll_y + margin_y, call->curve);
          // paintCurve(call->selectedCui, touch.px-1, touch.py,false);
        } 
          
      } else if (hkeys & KEY_B && call->selectedCui && call->selectedCui->next) {
        // if we go out of bounds we keep changing the repeat
        if (touch.px > call->selectedCui->next->x - scroll_x + margin_x) {
          adjustRepeat(call->selectedCui,touch.px -call->selectedCui->next->x,call->curve);
        } else if (touch.px < call->selectedCui->x - scroll_x + margin_x) {
          adjustRepeat(call->selectedCui,touch.px -call->selectedCui->x,call->curve);
        } else {
          adjustRepeat(call->selectedCui,dx,call->curve);
        }
        if (touch.py > 185) {
          adjustIntensity(call->selectedCui,7-(touch.py-192),call->curve);
        } else if (touch.py < 7) {
          adjustIntensity(call->selectedCui,touch.py,call->curve);
        } else {
          adjustIntensity(call->selectedCui,dy,call->curve);
        }
        bakeCurves(call->selectedCui);
        // we do this so it's not too easy to move nodes by mistake if you just tap them
      } else if (touch_timer == 0 && call->selectedCui){
        if (keys & KEY_RIGHT) movement +=1;
        if (keys & KEY_LEFT) movement -=1;
        movement = clamp(DELICATE,movement,STRONG);
        // only delicate works for now
        shiftCui(call->selectedCui, touch.px-scroll_x-margin_x, touch.py - scroll_y + margin_y, call->curve, movement);
      }
      lastTouch = touch;
    }  else {
      // if we relax touch we don't want to remember the last one
      lastTouch.px = 0;
      lastTouch.py = 0;
    }
    //curve change
    if  (hkeys & KEY_R) {
      if (keys & KEY_LEFT) {
        call->curve = (MAX_CURVES+call->curve -1) % MAX_CURVES;
      } else if  (keys & KEY_RIGHT) {
        call->curve = (call->curve +1) % MAX_CURVES;
      } else if (keys & KEY_TOUCH) {
        start_sw = touch.px;
      } else if (hkeys & KEY_TOUCH) {
        int curr_sw = start_sw - touch.px;
        if (curr_sw < -5) {
          call->curve = (MAX_CURVES+call->curve -1) % MAX_CURVES;
          start_sw = touch.px;
        } else if (curr_sw > 5) {
          call->curve = (call->curve +1) % MAX_CURVES;
          start_sw = touch.px;
        }
      }
    }

    if (keys & KEY_TOUCH) lastTouch = touch;

    glFlush(0);
	}
}