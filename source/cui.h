#include <nds.h>
#define MAX_CUI_X 1024
#define MAX_CUI_RES 512
#define DELICATE 0
#define LOCKED 1
#define STABLE 2
#define STRONG 3
#define MAX_CUI_CURVES 5
#define X_SELECT 7
#define Y_SELECT 7
struct Curve {
  // starting y point of the curve
  u16 y;
  // curve parameters
  u8 intensity;
  u8 angle;
  u8 bias;
  //TODO : add triangle 
  u16 repeat;
  // 2D table for a curve !!
  // first dim is x
  // second is y,
  u16 values[2][MAX_CUI_RES];
  // y offset (user drawn, not generated)
  s16 offset[MAX_CUI_RES];
  //  bypass custom parameter (for pitch it's mute or not, for pan has no use, for volume  for automation it could be used to turn off filter etc...)
  // bool bypass[MAX_CUI_RES];
  // external color component controlled by w
};

struct Curve  makeCurve() {
  struct Curve curve; 
  curve.angle=0;
  curve.bias=127;
  curve.repeat=inttof32(1.0);
  curve.intensity=0;
  return curve;
}



// these are the little sound nodes 
typedef struct CuiS {
  u8 num;
  u16 x;
  // curve parameters
  // curves
  // x and y are interleaved
  u16 curve_res;
  // first two curves are reserved for volume and pan
  // then lfo strength and freq,
  // next can be used for custom automations when we implement synth writing
  struct Curve curves[MAX_CUI_CURVES];
  
  
  // UI
  bool selected;
  bool sub_selected;
  //sound
  bool playing;
  u8 volume;
  s32 progress;
  // node
  struct CuiS* next;
  struct CuiS* prev;
} Cui;


s16 getY(Cui * cui,u8 c) {
  return cui->curves[c].y;
}


s32 tempo = floattof32(0.6);

s32 prog_speed = floattof32(0.001);
void makeCui( Cui * cui,u8 n, s16 x, s16 y , Cui * prev) {
  if (cui == NULL) return;
  cui->num=n;
  cui->x=x;

  cui->selected=false;
  cui->sub_selected=false;

  cui->playing = false;
  cui->volume   = 125;
  cui->progress = floattof32(0.0);
  
  cui->curve_res=50;

  for (int i = 0; i < MAX_CUI_CURVES; i++) {
    cui->curves[i]= makeCurve();
    if (i == 0) {
      cui->curves[i].y =y;
    } else if  (i==1) { // vol// 
      cui->curves[i].y = 92;
      cui->curves[i].intensity=0;
    } else if (i==2) { // pan
      cui->curves[i].y = 92;
      cui->curves[i].intensity=0;
    } else {
      cui->curves[i].y = 191;
      cui->curves[i].intensity=0;
    }
  }
  cui->prev = prev;
  if (prev !=NULL) {
    prev->next = cui;
  }
}
// new sine based curve  bake. 
void bakeCurves(Cui * cui) {
  // clauses for immediate return:
  // no cui. no next cui. previous cui is ahead. next cui is behind.
  if (!cui|| !cui->next || cui->prev && cui->prev->x > cui->x || cui->next->x < cui->x ) return;
  cui->curve_res = cui->next->x - cui->x;
  // we make sure we have at least 1 so we avoid division errors
  if (cui->curve_res <=0){ 
    cui->curve_res = 1;
    
    for (int c = 0; c < MAX_CUI_CURVES; c ++) {
      struct Curve * curve = &cui->curves[c]; 
      curve->values[0][0] = cui->x;
      curve->values[1][0] = curve->y;
      return;
    }
  }
    // t is the parameter for the sine
  s16 t;
    // i is the number of iterations
  int i;
  for (int c = 0; c < MAX_CUI_CURVES; c ++) {
    struct Curve * curve = &cui->curves[c]; 
    i = 0;
    t = 0;
    while (t < one && i < cui->curve_res) {
      //first we set up the base
      s16 base_y =mulf32((one-t),curve->y) + mulf32(t,getY(cui->next,c));
      s16 base_x =mulf32((one-t),cui->x) + mulf32(t,cui->next->x);
      // then get the sin of the curve
      //standard lerp serves as the base 
      t += divf32(1,(cui->curve_res));
      u16 sin = f32toint(
        mulf32(
          // repeat is here to multiply as needed. angle changes the starting angle
          sinLerp(mulf32(curve->repeat,degreesToAngle(360)*(divf32(curve->angle,255))+mulf32(t,degreesToAngle(360)))),
          // intensity rules the amplitude 
          inttof32(curve->intensity)));
      // X i
      curve->values[0][i] = base_x;
      curve->values[1][i] = base_y-sin;
      // curve->offset[i] = 0;//base_y-sin;
      // we don't touch the offset
      // curve.offset[i]=0;
      i += 1;
    }
  }
}
bool selectCui(Cui * cui, int px, int py, u8 curve_num) {    
  s16 diff_x = cui->x - px;
  s16 diff_y = getY(cui,curve_num) - py;
  if (diff_y < 0) {
      diff_y = -diff_y;
  }
  if (diff_x < 0) {
    diff_x = -diff_x;
  }
  cui->selected = diff_x < X_SELECT && diff_y < Y_SELECT;
  return cui->selected;
}

void forceAdvance(Cui * cui, int advance) {
  if (!cui->next) return;
  cui->progress +=advance;
  if (cui->progress > cui->next->x) {
    cui->progress=0;
    cui->playing=false;
    if (cui->next->next)
      cui->next->playing=true;
  }
}

void advance(Cui * cui) {
  if (!cui->next) return;
  cui->progress+=mulf32(prog_speed,tempo);
  if (cui->progress > cui->curve_res) {
    cui->progress=0;
    cui->playing=false;
    if (cui->next->next)
      cui->next->playing=true;
  }
}


void swapCuis(Cui * cui1 , Cui * cui2) {
  if (cui2 != cui1->next) {
    // iprintf("you can't swap these !");
    return;
  }
  cui1->next = cui2->next;
  cui2->next = cui1;
  cui2->prev = cui1->prev;
  if (cui2->prev) {
    cui2->prev->next = cui2;
  }
  cui1->prev = cui2;
  if (cui1->next) {
    cui1->next->prev = cui1;
  }
  cui1->num+= cui2->num;
  cui2->num=cui1->num-cui2->num;
  cui1->num-= cui2->num;
}


void moveCui(Cui * cui, int rdx, int dy, u8 curve_num,u8 level) {

  if (!cui) return;
  iprintf("moving cui %01i on %02i\n ",cui->num, (curve_num));
  int dx = rdx;
  // we only allow to swap with one node in one frame
  if (cui->next) {
    dx = min(dx,cui->next->x-cui->x);
  }
  if (cui->prev) {
    dx = max(dx,cui->prev->x-cui->x);
  }
  // iprintf("ah");
  // we only move horizontally if we don't go out of curve resolution limits
  if (!cui->next || cui->next->x -(cui->x + dx) < MAX_CUI_RES-1) {
    if (!cui->prev || cui->x + dx -(cui->prev->x) < MAX_CUI_RES-1){
      cui->x = clamp (0,cui->x+dx,MAX_CUI_X);
    } 
  } else {
  }
  // iprintf("oh");

  cui->curves[curve_num].y += dy;
    bakeCurves(cui);
    bakeCurves(cui->prev);

  // node passes through each other
  if (level == DELICATE) {
    iprintf("delicately bro\n");
    if (cui->next && cui->next->x <= cui->x) {
      iprintf("bru");
      swapCuis(cui,cui->next);
      bakeCurves(cui->prev);
      bakeCurves(cui);
      cui->x++;
    }
    if (cui->prev && cui->prev->x >= cui->x) {
      iprintf("bra");
      swapCuis(cui->prev,cui);
      bakeCurves(cui->next);
      bakeCurves(cui);
      cui->x--;
    }


  // node pushes neighbours
  } else if (level==STABLE) {
    iprintf("normally\n");
    if (cui->next && cui->next->x <= cui->x) {
      
      moveCui(cui->next,dx,0,curve_num,STABLE);
      bakeCurves(cui->next);
    }
    if (cui->prev && cui->prev->x >= cui->x) {
      moveCui(cui->prev,dx,0,curve_num,STABLE);
      bakeCurves(cui->prev);
    }
  // node pushes everyone 
  //  BUG : CURRENTLY THIS BREAKS EVERYTHING VERY EASILY AND I DON'T KNOW WHY !!!!!
  } else if (level == STRONG){
    // iprintf("strongly\n");
    if (dx > 0 && cui->next) {
      moveCui(cui->next,dx,0,curve_num,STRONG);
    } else if (dx <0 && cui->prev) {
      moveCui(cui->prev,dx,0,curve_num,STRONG);
    }

  }

  iprintf("moved.");
}

void shiftCui(Cui * cui, int px, int py, u8 curve_num,u8 level) {
  moveCui(cui, px-cui->x, py-getY(cui,curve_num), curve_num,level);
}

void adjustIntensity(Cui * cui, int delta,u8 curve_num) {
  if (cui==NULL) return;
  cui->curves[curve_num].intensity = clamp(0, cui->curves[curve_num].intensity-delta,255);
}

void adjustRepeat(Cui * cui, int delta, u8 curve_num) {
  if (cui==NULL) return;
  // handpicked multiplier to have coherent adjustment
  cui->curves[curve_num].repeat += divf32(inttof32(delta),inttof32(200));
}

void getPlayerPosition(Cui * cui, int * px, int * py, int *oy, u8 curve_num) {
  // *px = cui->progress+cui->x; value[2]
  *px = cui->curves[curve_num].values[0][cui->progress];
  *py = cui->curves[curve_num].values[1][cui->progress];
  if (oy) *oy = cui->curves[curve_num].offset[cui->progress];
}

// works fine for now
void paintCurve(Cui * cui, int px, int py, u8 curve_num) {
  // clauses for bypassing the function:
  // no cui. no next cui. 
  if (!cui || !cui->next ) return;
  // invalid curve num
  if (curve_num < 0 || curve_num > MAX_CUI_CURVES) {
    // iprintf("invalid curve number !!!!!!!!!!\n");
    return;
  }

  s16 curve_index = (px-cui->x);
  // the index makes the res out of bounds.
  if (curve_index > cui->curve_res) {
    paintCurve(cui->next,px,py,curve_num);
    // iprintf("drawing out of curve");
    return;
  }
  if (curve_index < 0) {
    if (cui->prev) {
      paintCurve(cui->prev,px,py,curve_num);
    }
    return;
  }
  s16 offset = cui->curves[curve_num].values[1][curve_index] - py;

  cui->curves[curve_num].offset[curve_index] = offset;
    
  bakeCurves(cui);
  // iprintf("pitch_curve drawing, on %03i\n, new offset : %03i",curve_index,offset);
}

// void mute(Cui * cui, int px, int py) {
//   // clauses for bypassing the function:
//   // no cui. no next cui. the px is before the pitch_curve. 
//   if (!cui || !cui->next || cui->x > px ) return;

//   s16 curve_index = (px-cui->x);
//   // the index makes the res out of bounds.
//   if (curve_index > cui->curve_res) return;
//   cui->mute[pitch_curve] = true;
// }

