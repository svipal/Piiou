#include <nds.h>
s32 powf32(s32 number, int power) {
  s32 accum =  floattof32(1.0);
  for (int i = power; i > 0; i--) {
    accum = mulf32(accum, number);
  }
  return accum;
}

int abs(int x) {
  if (x > 0) return x;
  return -x;
}

int min(int x, int y ){
  if (x<y) return x;
  return y;
}

int max(int x, int y){
  if (x<y) return y;
  return x;
}

int clamp(int l, int x, int h) {
  if (x > h) return h;
  if (x < l) return l;
  return x;
}

s16 half = floattof32(0.5); 
s16 one = floattof32(1.0);
s16 four = floattof32(4); 
s16 eight = floattof32(8); 
s16 two = floattof32(2);

/// old "epic" triangle 
// int triFunc(s16 angle) {

//   s16 modangle =(angle * 2)% degreesToAngle(360);
//   if (modangle > 0) {
//     return eight-modangle-four;
//   } else {
//     return  modangle-four;
//   }
// }

int triFunc(s16 angle) {
  s16 modangle =(angle/2);
  if (modangle < 0) {
    return -modangle+one;
  } else {
    return  modangle;
  }
}


int squFunc(s16 angle, u8 duty ) {
  s16 modangle =(angle)% degreesToAngle(360);
  if (modangle > degreesToAngle(f32toint(divf32(720*duty,100))-360)) {
    return inttof32(1);
  } else { 
    return inttof32(-1);
  }
}

// -360
// -180
// 0
// 180
// 360

