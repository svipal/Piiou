

//frame buffer pixel setting
void setPixel(u16 * memory, u8 px, u8 py, u16 color) {
  memory[(px % 256) + (py % 192) * 256] = color;
}


void drawBox(u16 * memory,bool filled, u8 x, u8 y, u8 x2, u8 y2, u16 color) {
  for (int i = x; i <= x2; i++){
    for (int j = y; j <= y2; j++) {
      if (filled || i == x || j == y || j == y2 || i == x2 ) {
        setPixel(memory,i,j,color);
      }
    }
  }
}
